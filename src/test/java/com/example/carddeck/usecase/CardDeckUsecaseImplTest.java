package com.example.carddeck.usecase;

import com.example.carddeck.model.Card;
import com.example.carddeck.usecase.CardDeckUsecase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class CardDeckUsecaseImplTest {

    @Autowired
    private CardDeckUsecase cardDeckService;

    @BeforeEach
    public void setUp() {
        cardDeckService.fold();
        cardDeckService.shuffle();
    }

    @Test
    public void testShuffle() {
        Set<Card> dealtCards = new HashSet<>();
        cardDeckService.shuffle();
        int totalCards = Card.Suit.values().length * Card.Rank.values().length;
        for (int i = 0; i < totalCards; i++) {
            Card card = cardDeckService.dealCard();
            assertTrue(dealtCards.add(card), "Duplicate card found: " + card);
        }
    }
}
