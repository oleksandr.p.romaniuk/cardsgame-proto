package com.example.carddeck;

import com.example.carddeck.api.CardDeckController;
import com.example.carddeck.usecase.CardDeckUsecase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CardDeckController.class)
public class CardDeckControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CardDeckUsecase cardDeckUsecase;

    @Test
    public void testShuffle() throws Exception {
        mockMvc.perform(post("/api/deck/shuffle")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(cardDeckUsecase, times(1)).shuffle();
    }

    @Test
    public void testDealCard() throws Exception {
        mockMvc.perform(get("/api/deck/deal")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(cardDeckUsecase, times(1)).dealCard();
    }

    @Test
    public void testFold() throws Exception {
        mockMvc.perform(post("/api/deck/fold")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(cardDeckUsecase, times(1)).fold();
    }
}
