package com.example.carddeck.api;

import lombok.AllArgsConstructor;

import com.example.carddeck.model.Card;
import com.example.carddeck.usecase.CardDeckUsecase;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/api/deck")
public class CardDeckController {

    private final CardDeckUsecase cardDeckService;

    @PostMapping("/shuffle")
    public void shuffle() {
        cardDeckService.shuffle();
    }

    @GetMapping("/deal")
    public Card dealCard() {
        return cardDeckService.dealCard();
    }

    @PostMapping("/fold")
    public void fold() {
        cardDeckService.fold();
    }
}
