package com.example.carddeck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarddeckApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarddeckApplication.class, args);
	}

}
