package com.example.carddeck.gateway;

import com.example.carddeck.model.Card;

public interface Deck {
    void shuffle();
    Card dealCard();
    void fold();
}
