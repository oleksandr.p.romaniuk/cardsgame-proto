package com.example.carddeck.gateway.impl;

import com.example.carddeck.gateway.Deck;
import com.example.carddeck.model.Card;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Stack;

@Component
public class StandardDeck implements Deck {
    private Stack<Card> cards;

    @PostConstruct
    public void init() {
        cards = new Stack<>();
        populateDeck();
    }

    private void populateDeck() {
        for (Card.Suit suit : Card.Suit.values()) {
            for (Card.Rank rank : Card.Rank.values()) {
                cards.push(new Card(suit, rank));
            }
        }
    }

    @Override
    public void shuffle() {
        Collections.shuffle(cards);
    }

    @Override
    public Card dealCard() {
        return cards.pop();
    }

    @Override
    public void fold() {
        cards.clear();
        populateDeck();
    }
}
