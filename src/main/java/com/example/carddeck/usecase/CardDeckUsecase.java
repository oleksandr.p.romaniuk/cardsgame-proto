package com.example.carddeck.usecase;

import com.example.carddeck.model.Card;

public interface CardDeckUsecase {
    void shuffle();
    Card dealCard();
    void fold();
}
