package com.example.carddeck.usecase.impl;

import com.example.carddeck.gateway.Deck;
import com.example.carddeck.model.Card;
import com.example.carddeck.usecase.CardDeckUsecase;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CardDeckUsecaseImpl implements CardDeckUsecase {

    @Autowired
    private final Deck deck;

    @Override
    public void shuffle() {
        deck.shuffle();
    }

    @Override
    public Card dealCard() {
        return deck.dealCard();
    }

    @Override
    public void fold() {
        deck.fold();
    }
}
