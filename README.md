# cardsgame-proto

## Project status
we have a controller with three endpoints:

/api/deck/shuffle (POST): Shuffles the deck.
/api/deck/deal (GET): Deals a card from the deck.
/api/deck/fold (POST): Folds the deck (clears all cards).
To run the application, execute mvn spring-boot:run or run the Application.java class from your IDE. You can use a tool like Postman or Curl to make requests to the provided endpoints.
